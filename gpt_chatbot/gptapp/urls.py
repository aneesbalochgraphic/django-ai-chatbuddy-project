from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path("", views.home , name="home"),
    path("login_user", views.login_user, name="login_user"),
    path("create_account", views.create_account, name="create_account"),
    path("logout_user", views.logout_user, name="logout_user"),
    path("starter_view", views.starter_view, name="starter_view"),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
