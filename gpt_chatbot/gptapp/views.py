from django.shortcuts import render, redirect
from django.http import JsonResponse
from openai import OpenAI
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.conf import settings

# Create your views here.
# api key
def get_completion(prompt):
    print(prompt)
    query = openai.Completion.create(
        engine="text-davinci-003",
        prompt=prompt,
        max_tokens=1024,
        n=1,
        stop=None,
        temperature=0.5,
    )

    response = query.choices[0].text
    print(response)
    return response
client = OpenAI(
    api_key = settings.GPT_KEY
)

# home view
def home(request):
    if not request.user.is_authenticated:
        return redirect("starter_view")
    if request.method == "POST":
        prompt = request.POST.get("prompt")
        #response = get_completion(prompt)
        #return JsonResponse({"response":response})
        chat_completion = client.chat.completions.create(

            model="gpt-3.5-turbo",

            messages = [{
                "role":"user",
                "content":prompt,

            }],
        )
        #print(chat_completion.choices[0].message)
        response = chat_completion.choices[0].message

        return render (request, "gptapp/response.html",{"response":response})
        #print(response)

    return render (request, "gptapp/home.html")

# authentication of user
def login_user(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is None:
            messages.error(request, "Invalid Username or password")
            return redirect("login")
        else:
            login(request, user)
            return redirect("home")
    return render(request, "gptapp/login.html")

# create new user account func
def create_account(request):
    if request.method == "POST":
        username = request.POST["username"]
        email = request.POST["email"]
        password = request.POST["password"]

        # check if user already exist
        user = User.objects.filter(username=username)
        if user.exists():
            messages.info(request, "User name is already taken")
            return redirect("create_account")

        # create user account
        user = User.objects.create_user(
            username = username,
            email = email,
        )

        user.set_password(password)
        user.save()

        messages.success(request, "Account has been created")
        return redirect("create_account")


    return render(request, "gptapp/create_account.html")

# logout user func
def logout_user(request):
    logout(request)
    return redirect("login_user")

# createing a stater func
def starter_view(request):

    return render(request, "gptapp/start.html")
